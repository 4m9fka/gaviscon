import Vue from 'vue'
import Router from 'vue-router'
import Gallery from './views/Gallery.vue'
import About from './views/About.vue'
import Index from './views/Index.vue'
import Test from './views/Test.vue'
import Video from './views/Video.vue'


import Meta from 'vue-meta';
import SocialSharing from 'vue-social-sharing'
import VueMq from 'vue-mq'
import VueAwesomeSwiper from 'vue-awesome-swiper'


Vue.use(VueAwesomeSwiper, /* { default global options } */)
Vue.use(Router);
Vue.use(Meta);
Vue.use(SocialSharing);
import VueSilentbox from 'vue-silentbox'

Vue.use(VueSilentbox);



Vue.use(VueMq, {
    breakpoints: {
        mobile: 769,
        desktop: 1367,
        large: Infinity,
    }
});





import EventBus from './event-bus.js';

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'index',
            component: Index
        },
        {
            path: '/about',
            name: 'about',
            component: About
        },
        {
            path: '/gallery',
            name: 'gallery',
            component: Gallery
        },

        {
            path: '/test',
            name: 'test',
            component: Test
        },
        {
            path: '/video',
            name: 'video',
            component: Video
        },


    ]
});
router.beforeEach((to, from, next) => {
    if (from.name !== to.name){
        EventBus.$emit('start-loading');
    }
    setTimeout(next, 400)
});
router.afterEach((to, from) => {
    if (to.name !== "gallery") {
        EventBus.$emit('end-loading');
    }
});






//
import VueYandexMetrika from 'vue-yandex-metrika'
import VueAnalytics from 'vue-analytics'


Vue.use(VueYandexMetrika, {
    id: 41254644,
    router: router,
    env: process.env.NODE_ENV
});

Vue.use(VueAnalytics, {
    id: 'UA-88014128-22',
    router,
    autoTracking: {
        pageviewTemplate (route) {
            return {
                page: route.path + location.hash,
                title: document.title,
                location: window.location.href
            }
        }
    },
    // debug: {
    //   enabled: true,
    //   trace: false,
    //   sendHitTask: true
    //
    // }
});
export default router;
